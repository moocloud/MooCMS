Example Project Directory:

**root_projext /**
- content.html
- page.moocms.json
- main.moocms.json
 - **/page1**
   - content.html
   - page.moocms.json


 **BOLD**=directory 

---

file `root_projext/content.html` have this code inside of it:

    <a class="py-2 d-none d-md-inline-block i18n-1" href="/page1">Product</a>
    <a class="py-2 d-none d-md-inline-block i18n-1" href="https://status.moocloud.ch">MooCloud Status</a>
file `root_projext/page1/page.moocms.json` have this code inside of it:

	    "slug":{
                  "project":"/page1",
                  "it_IT":"/prodotto",
                  "en_EN":"/product"
               },
after the building the output of this project will be:

**/root**
 - **/it**
	 - index.html
	 - **/prodotto**
		 - index.html
 - **/en**
	  - index.html
	 - **/product**
		 - index.html

the code for the 2 button that we have analyse before will be:
For italian `/root/it/index.html` 

    <a class="py-2 d-none d-md-inline-block i18n-1" href="/it/prodotto">Prodotto</a>
    <a class="py-2 d-none d-md-inline-block i18n-1" href="https://status.moocloud.ch">MooCloud Status</a>

For English  `/root/en/index.html` 

    <a class="py-2 d-none d-md-inline-block i18n-1" href="/en/product">Product</a>
    <a class="py-2 d-none d-md-inline-block i18n-1" href="https://status.moocloud.ch">MooCloud Status</a>

