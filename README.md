
# MooCMS

CMS for Static Website or Pinegrow project. 


## Commands:

    moocms
      -g , --generate /main.moocms.json
      -b , --build /main.moocms.json

### moocms -g , --**generate** `[file path to main.moocms.json]`
1. This command will search all the html page and there corresponding json file. if the page doesn't have any json file it will generate the file. 
2. The generate will check for any `i18n` class to list them on the json file. 
### moocms -b , --**build** `[file path to main.moocms.json]`
1. This command will create the directory `build` in the root dir. if this directory exist it will cancel all the content. 
The script will also create the appropriate directory for every language present in the `main.moocms.json` file. 
2. The `assets` folder will be copy to the build folder.  

**Example**: main.moocms.json

    "static-contents":[
               "/assets",
               "/boostrap"],

3. the script start to build the page of the project creating a copy of the website for every language, using the translated text and the correct path for the page. 
The compiled page on the build folder will be named index.html. 



## General Info
The script uses .json files to manage the pages, their translations and speed up development of static site. 

A project that wants to run MotoCMS correctly, must include:

- `master.html` that contain html that need to be used on all page
- `main.moocms.json` that contain global information on the website

MooCMS will render only 1 HTML file for folder, the file need to be named `content.html`, combine with a `page.moocms.json` that will contain all the information on tha page, and the alternative translation for the page text. 

All the index.html file or page-name.html will be ignored by the script. 


### Multi-language

MooCMS will search every `div` in the page HTML with the class `i18n` and list them in the `page.moocms.json` file, after have them listed will change the class on the HTML file with `i18n-[number of the text]`   
#### **Example**:

**Before**:  
content.html:

    <a  class="py-2 d-none d-md-inline-block i18n"  href="/product">Product</a>

**After**:  
content.html:

    <a  class="py-2 d-none d-md-inline-block i18n-1"  href="/product">Product</a>
    
page.moocms.json:

    [...]
        "traslation":{
           "i18n-1":{
              "en_EN":"Product",
              "it_IT":"Translation_is_Missing"
           }
        }
    
### link management

During the execution of the `build` command, the script corrects the various links in the html with those chosen for the language that is compiling the code.
The script takes the information from the `page.motocms.json` files in the directory indicated in the page's html.

#### **Example**:

**Before compiling**:
content.html :

    <a  class="py-2 d-none d-md-inline-block i18n-1"  href="/product">Product</a>
    
the script will go to check the `page.moocms.json` in the /product directory.

    "slug":{
              "project":"/product",
              "it_IT":"/prodotto",
              "en_EN":"/product-live"
           },
 and it will replace the `/product`in the `href` with the corresponding path/link for the language that's trying to compile.